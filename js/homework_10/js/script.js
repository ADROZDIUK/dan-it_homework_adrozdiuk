let arrTeam = [20,4,22,14,9];
let arrGrooming = [222,48,66,55,44];
let deadlineTime = new Date("2019-03-15 00:00");
let dateNow = new Date();

let needDate = (startData, dateDeadline) => {  //deadline day is not inclusive

    let currentTime = startData;
    let haveWorkDays;
    currentTime.getHours() > 18 ? haveWorkDays = -1 : haveWorkDays = 0;
        while (dateDeadline >= currentTime) {
            if (currentTime.getDay() !== 0 && currentTime.getDate() !== 6) {
                haveWorkDays++;
            }
                currentTime.setDate(currentTime.getDate() + 1)
            }
    return haveWorkDays;
}

const tiktak =(team,tasksGrooming,haveDaysToWork)=>{
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    let oneDayCanTeam = team.reduce(reducer);
    let oneHourCanTeam = team.reduce(reducer) / 8;
    let allGroomingTime = tasksGrooming.reduce(reducer);
    let realProffit = oneDayCanTeam * haveDaysToWork;  //cant do storpoint team to deadline
    let needsDaysForTeamFork =  Math.ceil(allGroomingTime / oneDayCanTeam);
    // console.log(`real days to need: ${needsDaysForTeamFork}`);
    if(needsDaysForTeamFork > haveDaysToWork){
        let stillHourNeed =  Math.ceil((allGroomingTime - realProffit) / oneHourCanTeam);
        alert(`Команде разработчиков придется потратить дополнительно ${stillHourNeed} часов после дедлайна, чтобы выполнить все задачи в беклоге`); 
    } else if(needsDaysForTeamFork < haveDaysToWork) {
        alert(`Все задачи будут успешно выполнены за ${haveDaysToWork - needsDaysForTeamFork} дней до наступления дедлайна!`);
    } else {alert(`Вы как раз успеете в срок`)};
};
console.log(tiktak(arrTeam,arrGrooming,needDate(dateNow,deadlineTime)));