let primeNumberMin;
let primeNumberMax;
do {
    if(primeNumberMin !== undefined && primeNumberMax !== undefined){
        alert("Something went wrong. Please, enter the first minimum number(more than one), then the maximum number.");
    }
    primeNumberMin = Number(prompt("enter min number", ""));
    primeNumberMax = Number(prompt("enter max number", ""));
} while (primeNumberMin >= primeNumberMax || !Number.isInteger(primeNumberMax) || !Number.isInteger(primeNumberMin) || primeNumberMin <= 1);


    startAgain:for (let i = primeNumberMax; i >= primeNumberMin; i--) {
        for (let p = 2; p < i; p++) {
              if(i % p == 0) continue startAgain;
        } console.log("Prime number: " + i);
    }