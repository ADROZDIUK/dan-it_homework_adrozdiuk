
let promptNum =()=>{
    let pickNumber = Number(prompt("give me number",""));
    while (!(Number.isInteger(Number(pickNumber))&&(Number(pickNumber)>=1))) {
        pickNumber = prompt("Please, enter non-negative integer number(> or = 1)","");
    }
return Number(pickNumber);
}
let validNumber = promptNum();
let factorial =(a) =>{
    if(a !== 1){
        return a * factorial(a - 1);
    } else {
        return a;
    }
 }
document.write(`<span class="factorial">Factorial of number ${validNumber} is ${factorial(validNumber)}</span>`);
 
