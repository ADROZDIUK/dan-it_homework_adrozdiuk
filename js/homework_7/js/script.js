const usersO = [{
    name: "Ivan",
    surname: "Petrovich",
    gender: "male",
    age: 30,
  },
  {
    name: "Anna",
    surname: "Pozichkina",
    gender: "female",
    age: 44,
  },
  {
    name: "Alisha",
    surname: "Petrovich",
    gender: "female",
    age: 29,
  },
  {
    name: "Anna",
    surname: "Popova",
    gender: "female",
    age: 22,
  },
  {
    name: "Nina",
    surname: "Anna",
    gender: "female",
    age: 31,
  }];

  const usersZ = [{
    name: "Alex",
    surname: "Petrovich",
    gender: "male",
    age: 27,
  },
  {
    name: "Anna",
    surname: "Kozlova",
    gender: "female",
    age: 25,
  },
  {
    name: "Inna",
    surname: "Anna",
    gender: "female",
    age: 72,
  },
  {
    name: "Anna",
    surname: "Frolova",
    gender: "female",
    age: 15,
}];

const fieldName = "name";

let excludeBy =(arr1,arr2,nameFindProp) => {
    let newArr = [];
    let filterInArr2 = [];

    arr2.forEach(function(itemInArr2) {
        for(let propInArr2 in itemInArr2){
            if(propInArr2 === nameFindProp){
                filterInArr2.push(itemInArr2[propInArr2]); //пушим в массив все значения nameFindProp , которые нашли в arr2
                filterInArr2 = [...new Set(filterInArr2)]; // используем set для фильтрации на уникальные значения в массиве
            };
        }
    });
    
    arr1.forEach(function(itemInArr1) {
        let i  = filterInArr2.length;
            while (i--){
                if(itemInArr1[nameFindProp] === filterInArr2[i]){  //если значения в буферном массиве filterInArr2 и значением свойства nameFindProp в arr1 не совпадают, то:
                    newArr.push(itemInArr1); // тут я пушу в новый массив те объекты название свойства которых совпадает с найденым в отфильтрованом массиве.
                }
            }
    });
    newArr = arr1.filter(x => !newArr.includes(x)); //создает новый массив на основании сравнения каждого элемента arr1[x] который не содержится в newArr
    return newArr;
};
console.log(excludeBy(usersO,usersZ,fieldName));