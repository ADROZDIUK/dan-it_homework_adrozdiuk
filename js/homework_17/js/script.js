$(document).ready(function(){
    const button = $('#draw_circle');
    const inputDiameter = $(`<input type="text" id="diameter" placeholder="enter diameter circle">`);
    const draw = $('<button>draw circle</button>');
    const body = $(`body`);

        $(button).on('click', function() {
            button.hide();
            createInput();
        });

        const createInput = ()=> {
            $(body).append(inputDiameter);
            $(body).append(draw);
            $(inputDiameter).css({backgroundColor: 'aqua'});
        };

        $(draw).on('click', function(){
            $(body).children().hide();
            createCircle(inputDiameter.val())
        })

        let wrapper = $(`<div class="list-ofcircle"></div>`);

        const createCircle = (size) => {
            $(wrapper).css({width:`${size*10}px`,display: "flex", flexWrap: "wrap"});
            $(body).append(wrapper);

            for(let i = 0; i < 100; i++ ) {
                let circle = $(`<div class="color_circle"></div>`);
                let colorOfCircle = getRandomColor();
                $(circle).css({backgroundColor: `${colorOfCircle}`, width:`${size}px`, height:`${size}px`,borderRadius: '50%'})
                $(wrapper).append(circle); 

            };
        };

        let getRandomColor = () => {
            let letters = '0123456789ABCDEF'.split('');
            let color = '#';
            for (let i = 0; i < 6; i++ ) {
                color += letters[Math.round(Math.random() * 15)];
            }
            return color;
        }

        wrapper.on('click', function(event){
            let target = $(event.target);
            if(target.hasClass("color_circle")) {
                $(target).hide();
            }
        });
});