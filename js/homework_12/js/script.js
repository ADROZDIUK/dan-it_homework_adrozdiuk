const button = document.getElementById('draw_circle');
const inputDiameter = document.createElement('input');
const draw = document.createElement('button');

button.onclick =() => {
    button.hidden = true;
    createInput()
};

const createInput = ()=> {
    document.body.appendChild(inputDiameter).setAttribute("id", "diameter");
    inputDiameter.setAttribute('placeholder',"enter diameter circle");
    document.body.appendChild(draw).prepend("draw circle");
    draw.style.cssText = `background-color: aqua`;
};

draw.onclick = () => {
    for (let i = 0; i < document.body.childNodes.length; ++i) {
        document.body.childNodes[i].hidden = true;
    };
    createCircle(inputDiameter.value)
};

let wrapper = document.createElement('div');

const createCircle = (size) => {
    wrapper.classList.add('list-ofcircle'); 
    wrapper.style.cssText = `width: ${size*10}px;
    display: flex;
    flex-wrap: wrap;`;
    document.body.appendChild(wrapper);

    for(let i = 0; i < 100; i++ ) {
        const circle = document.createElement('div');
        let colorOfCircle = getRandomColor();
        circle.classList.add('color_circle');
        circle.style.cssText = `background-color: ${colorOfCircle};
        width: ${size}px;
        height: ${size}px;    
        border-radius:50%;`
        wrapper.appendChild(circle); 
    };
};

let getRandomColor = () => {
    let letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

wrapper.addEventListener('click',function(event){
    event.target.hidden = true;
});