let myBtn = document.getElementById('myBtn');
let contactBtn = document.getElementsByClassName('tfooter_a-1')[0];
let localValue = localStorage.getItem('colorSite');

const ready = function() {
    runColor();
};

const runColor = () => {
    if(localValue === "0" || localValue === null) {
        contactBtn.style.cssText = `color: #FFF;
        background: #484848;`;
        document.body.style.cssText = `background:#fff;`;
    } else if (localValue === "1") {
        contactBtn.style.cssText = `color: #000;
        background: #fff;`;
        document.body.style.cssText = `background:#000;`;
    };
};

const changeColor = () => {
    if(localValue === "0" || localValue === null) {
        localStorage.setItem('colorSite', '1');
        localValue = localStorage.getItem('colorSite');
    } else if(localValue === "1"){
        localStorage.setItem('colorSite', '0');
        localValue = localStorage.getItem('colorSite');
    };
    runColor();
}
document.addEventListener("DOMContentLoaded", ready);
myBtn.onclick = changeColor;

