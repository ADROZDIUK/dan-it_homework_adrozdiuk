const myObj = {
  a: "ssdasdas",
  b: {а: 5, b: undefined},
  c: null,
  zz: /\w+/,
  g: new Date(),
  d: {name: "Berlin",population: 3375000,year: 1237},
  е: 5552232,
  f: {a:{a:"hahaha,classic",b:true,c:new Date()}},
}

let cloneObject=(obj)=>{
const cloneObj = {};
  for (let prop in obj){
    if(typeof(prop)=="object" && prop != null) {
      if(obj[prop] instanceof Date) {  
        let cloneDate =  new Date(obj[prop].getTime());
        cloneObj[prop] = cloneDate;
      } else if(obj[prop] instanceof RegExp) {
        let cloneRegExp =  new RegExp(obj[prop]);
        cloneObj[prop] = cloneRegExp;
      } else {
        cloneObj[prop] = cloneObject(obj[prop]);  
      };
    } else {
      cloneObj[prop] = obj[prop];
    };
  }
  return cloneObj;
}
const cloneObj  = cloneObject(myObj);
console.log(cloneObj);