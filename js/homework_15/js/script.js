const stopwatch = document.getElementsByClassName('stopwatch');

[].forEach.call(stopwatch, function () {
    let currentTimer = 0,
        interval = 0,
        lastUpdateTime = new Date().getTime(),
        start = document.getElementsByClassName('start')[0];
        pause = document.getElementsByClassName('pause')[0];
        clear = document.getElementsByClassName('clear')[0];
        mins = document.getElementsByClassName('minutes')[0];
        secs = document.getElementsByClassName('seconds')[0];
        cents = document.getElementsByClassName('centiseconds')[0];

    let pad = (n) => {
        console.log(n);
        return ('0000000000' + n).substr(-2);
    }

    const update = () => {
        let now = new Date().getTime(),
            dt = now - lastUpdateTime;

        currentTimer += dt;

        let time = new Date(currentTimer);

        mins.innerText = pad(time.getMinutes());
        secs.innerText = pad(time.getSeconds());
        cents.innerText = pad(Math.floor(time.getMilliseconds() / 10));

        lastUpdateTime = now;
    }

    start.addEventListener('click', () => {
        if (!interval) {
            lastUpdateTime = new Date().getTime();
            interval = setInterval(update, 1);
        }
        start.hidden = true;
        pause.hidden = false;
    });

    pause.addEventListener('click', stopTimer = () => {
        clearInterval(interval);
        interval = 0;
        start.hidden = false;
        pause.hidden = true;
    });

    clear.addEventListener('click', () => {
        stopTimer();
        currentTimer = 0;
        mins.innerText= secs.innerText = cents.innerText = pad(0);
    });
});

